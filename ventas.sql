-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 17-05-2021 a las 23:39:59
-- Versión del servidor: 10.4.19-MariaDB
-- Versión de PHP: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ventas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `persona_id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `edad` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `provincia` varchar(100) NOT NULL,
  `correo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`persona_id`, `nombre`, `apellido`, `edad`, `fecha`, `provincia`, `correo`) VALUES
(1, 'Sergio', 'Ortiz', 95896593, '2021-05-11', 'Caracas', 'Sergio@gmail.com'),
(9, 'Romi', 'Gil', 587564, '2021-05-20', 'Cordoba', 'lkajhdlkadjs'),
(12, 'Sergio', 'Ortiz', 454, '2021-05-06', 'Cordoba', 'lkajhdlkadjs'),
(13, 'Sergio', '', 474, '2021-05-12', 'Cordoba', 'lkajhdlkadjs'),
(14, 'Sergio', '', 474, '2021-05-12', 'Cordoba', 'lkajhdlkadjs'),
(15, 'Sergio', '', 566, '2021-05-12', 'Cordoba', 'lkajhdlkadjs'),
(18, 'Romi', 'Ortiz', 455646541, '2021-05-11', 'Cordoba', 'lkajhdlkadjs'),
(19, 'Brisa', 'Maylen', 1561, '2021-05-18', 'Cordoba', 'lkajhdlkadjs'),
(21, 'Brisa', 'Maylen', 54151, '2021-05-19', '', 'lkajhdlkadjs'),
(22, 'Romi', 'Gil', 15135, '2021-05-20', 'Cordoba', 'iojeljkd'),
(23, 'Gaby', 'Mendoza', 215648, '2021-05-19', 'Cordoba', 'ewerwf'),
(27, 'Mama', 'Betty', 1351, '2021-05-25', 'Cordoba', 'betty@gmail.com'),
(28, 'Mama', 'Betty', 1351, '2021-05-25', 'Cordoba', 'betty@gmail.com'),
(29, 'Mama', 'Betty', 1351, '2021-05-25', 'Cordoba', 'betty@gmail.com'),
(30, 'Mama', 'Betty', 1351, '2021-05-25', 'Cordoba', 'betty@gmail.com'),
(31, 'Mama', 'Betty', 1351, '2021-05-25', 'Cordoba', 'betty@gmail.com'),
(32, 'Gaby', 'Perez', 545, '2021-05-11', 'Cordoba', 'betty@gmail.com'),
(33, 'Gaby', 'Perez', 545, '2021-05-11', 'Cordoba', 'betty@gmail.com'),
(34, 'Ramon', 'Perez', 545, '2021-05-11', 'Cordoba', 'betty@gmail.com'),
(37, 'Alicia', 'Mendoza', 21414106, '2021-05-11', 'Cordoba', 'betty@gmail.com'),
(41, 'Sergio', 'Gil', 21414106, '2021-05-19', 'Cordoba', 'betty@gmail.com'),
(42, 'Antonia', 'Perez', 147258, '2021-05-20', 'Buenos Aires', 'Antonia22@correo.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `codigo` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `precioVenta` decimal(5,2) NOT NULL,
  `precioCompra` decimal(5,2) NOT NULL,
  `existencia` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `codigo`, `descripcion`, `precioVenta`, `precioCompra`, `existencia`) VALUES
(1, '1', 'Galletas chokis', '15.00', '10.00', '100.00'),
(2, '2', 'Mermelada de fresa', '80.00', '65.00', '100.00'),
(3, '3', 'Aceite', '20.00', '18.00', '100.00'),
(5, '5', 'Doritos', '8.00', '5.00', '100.00'),
(6, '6', 'Mate Cocido', '120.00', '80.00', '100.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_vendidos`
--

CREATE TABLE `productos_vendidos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_producto` bigint(20) UNSIGNED NOT NULL,
  `cantidad` bigint(20) UNSIGNED NOT NULL,
  `precio` decimal(5,2) NOT NULL,
  `id_venta` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos_vendidos`
--

INSERT INTO `productos_vendidos` (`id`, `id_producto`, `cantidad`, `precio`, `id_venta`) VALUES
(3, 1, 10, '15.00', 2),
(4, 1, 1, '15.00', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id`, `fecha`) VALUES
(2, '2021-05-17 15:22:19'),
(3, '2021-05-17 15:28:02');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`persona_id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos_vendidos`
--
ALTER TABLE `productos_vendidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_producto` (`id_producto`),
  ADD KEY `id_venta` (`id_venta`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `persona_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `productos_vendidos`
--
ALTER TABLE `productos_vendidos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `productos_vendidos`
--
ALTER TABLE `productos_vendidos`
  ADD CONSTRAINT `productos_vendidos_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `productos_vendidos_ibfk_2` FOREIGN KEY (`id_venta`) REFERENCES `ventas` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
